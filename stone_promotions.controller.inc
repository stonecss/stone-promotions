<?php

class PromotionController extends EntityAPIController {
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'title' => '',
      'cta' => '',
      'url' => '',
      'days' => '',
      'status' => 0,
      'uid' => $user->uid,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  function attachLoad(&$queried_promotions, $revision_id = FALSE) {
    foreach ($queried_promotions as &$promotion) {
      if (is_string($promotion->days)) {
        $promotion->days = array_intersect_key(stone_promotions_days_of_the_week(), array_flip(explode(',', $promotion->days)));
      }
      else {
        $promotion->days = array();
      }
    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_user_load().
    parent::attachLoad($queried_promotions, $revision_id);
  }
}

class PromotionTypeController extends EntityAPIControllerExportable {
  public function create(array $values = array()) {
    $values += array(
      'label' => '',
      'status' => 0,
    );

    return parent::create($values);
  }

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * UI controller for promotion Type.
 */
class PromotionTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage promotion Types.';
    return $items;
  }
}

/**
 * Promotion class.
 */
class Promotion extends Entity {
  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'promotion/' . $this->identifier());
  }
}

/**
 * Promotion Type class.
 */
class PromotionType extends Entity {
  public $type;
  public $label;
  public $weight = 0;

  public function __construct($values = array()) {
    parent::__construct($values, 'promotion_type');
  }

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
