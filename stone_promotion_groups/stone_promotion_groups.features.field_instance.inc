<?php
/**
 * @file
 * stone_promotion_groups.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function stone_promotion_groups_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'promotion_group-slideshow-field_promotion_refs'
  $field_instances['promotion_group-slideshow-field_promotion_refs'] = array(
    'bundle' => 'slideshow',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => array(
          'links' => 0,
          'view_mode' => 'full',
        ),
        'type' => 'entityreference_entity_view',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'promotion_group',
    'field_name' => 'field_promotion_refs',
    'label' => 'Promotions',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Promotions');

  return $field_instances;
}
