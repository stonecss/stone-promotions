<?php
/**
 * @file
 * stone_promotion_groups.features.inc
 */

/**
 * Implements hook_views_api().
 */
function stone_promotion_groups_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_promotion_group_type().
 */
function stone_promotion_groups_default_promotion_group_type() {
  $items = array();
  $items['slideshow'] = entity_import('promotion_group_type', '{
    "type" : "slideshow",
    "label" : "Slideshow",
    "weight" : 0,
    "description" : ""
  }');
  return $items;
}
