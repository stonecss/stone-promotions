<?php
/**
 * @file
 * stone_promotion_groups.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function stone_promotion_groups_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'promotion_entity_reference';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'promotion';
  $view->human_name = 'Promotion Entity Reference';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_promotion_refs_promotion_group']['id'] = 'reverse_field_promotion_refs_promotion_group';
  $handler->display->display_options['relationships']['reverse_field_promotion_refs_promotion_group']['table'] = 'promotion';
  $handler->display->display_options['relationships']['reverse_field_promotion_refs_promotion_group']['field'] = 'reverse_field_promotion_refs_promotion_group';
  /* Field: Promotion: Promotion ID */
  $handler->display->display_options['fields']['pid']['id'] = 'pid';
  $handler->display->display_options['fields']['pid']['table'] = 'promotion';
  $handler->display->display_options['fields']['pid']['field'] = 'pid';
  $handler->display->display_options['fields']['pid']['label'] = '';
  $handler->display->display_options['fields']['pid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['pid']['element_label_colon'] = FALSE;
  /* Field: Promotion: Image */
  $handler->display->display_options['fields']['field_promotion_image']['id'] = 'field_promotion_image';
  $handler->display->display_options['fields']['field_promotion_image']['table'] = 'field_data_field_promotion_image';
  $handler->display->display_options['fields']['field_promotion_image']['field'] = 'field_promotion_image';
  $handler->display->display_options['fields']['field_promotion_image']['label'] = '';
  $handler->display->display_options['fields']['field_promotion_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_promotion_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_promotion_image']['settings'] = array(
    'image_style' => 'media_thumbnail',
    'image_link' => '',
  );
  /* Field: Promotion: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'promotion';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'pid' => 'pid',
    'title' => 'title',
    'field_promotion_image' => 0,
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['row_options']['separator'] = ':';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $translatables['promotion_entity_reference'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Promotion Group referencing Promotion from field_promotion_refs'),
    t('.'),
    t(','),
    t('Entity Reference'),
  );
  $export['promotion_entity_reference'] = $view;

  $view = new view();
  $view->name = 'stone_promotion_groups_admin';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'promotion_group';
  $view->human_name = 'Promotion groups';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Promotion groups';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'pgid' => 'pgid',
    'views_bulk_operations' => 'views_bulk_operations',
    'title' => 'title',
    'field_promotion_refs' => 'field_promotion_refs',
    'status' => 'status',
    'changed' => 'changed',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = 'changed';
  $handler->display->display_options['style_options']['info'] = array(
    'pgid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_promotion_refs' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Promotion Group: Promotion group ID */
  $handler->display->display_options['fields']['pgid']['id'] = 'pgid';
  $handler->display->display_options['fields']['pgid']['table'] = 'promotion_group';
  $handler->display->display_options['fields']['pgid']['field'] = 'pgid';
  $handler->display->display_options['fields']['pgid']['exclude'] = TRUE;
  /* Field: Bulk operations: Promotion Group */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'promotion_group';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '1';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          '_all_' => '_all_',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::panelizer_set_status_action' => array(
      'selected' => 0,
      'postpone_processing' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Field: Promotion Group: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'promotion_group';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  /* Field: Promotion Group: Promotions */
  $handler->display->display_options['fields']['field_promotion_refs']['id'] = 'field_promotion_refs';
  $handler->display->display_options['fields']['field_promotion_refs']['table'] = 'field_data_field_promotion_refs';
  $handler->display->display_options['fields']['field_promotion_refs']['field'] = 'field_promotion_refs';
  $handler->display->display_options['fields']['field_promotion_refs']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_promotion_refs']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_promotion_refs']['multi_type'] = 'ol';
  /* Field: Promotion Group: Status */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'promotion_group';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  /* Field: Promotion Group: Date changed */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'promotion_group';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['label'] = 'Updated';
  $handler->display->display_options['fields']['changed']['date_format'] = 'short';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="/promotion-group/[pgid]">View</a> |
<a href="/promotion-group/[pgid]/edit">Edit</a> |
<a href="/promotion-group/[pgid]/delete">Delete</a>';
  $translatables['stone_promotion_groups_admin'] = array(
    t('Master'),
    t('Promotion groups'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Promotion group ID'),
    t('.'),
    t(','),
    t('Promotion Group'),
    t('- Choose an operation -'),
    t('Title'),
    t('Promotions'),
    t('Status'),
    t('Updated'),
    t('Operations'),
    t('<a href="/promotion-group/[pgid]">View</a> |
<a href="/promotion-group/[pgid]/edit">Edit</a> |
<a href="/promotion-group/[pgid]/delete">Delete</a>'),
  );
  $export['stone_promotion_groups_admin'] = $view;

  return $export;
}
