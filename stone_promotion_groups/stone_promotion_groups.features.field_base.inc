<?php
/**
 * @file
 * stone_promotion_groups.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function stone_promotion_groups_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_promotion_refs'
  $field_bases['field_promotion_refs'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_promotion_refs',
    'foreign keys' => array(
      'promotion' => array(
        'columns' => array(
          'target_id' => 'pid',
        ),
        'table' => 'promotion',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'views',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'view' => array(
          'args' => array(),
          'display_name' => 'entityreference_1',
          'view_name' => 'promotion_entity_reference',
        ),
      ),
      'target_type' => 'promotion',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
