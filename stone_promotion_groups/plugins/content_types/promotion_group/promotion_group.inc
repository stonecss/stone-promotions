<?php

/**
 * @file
 * Provide promotions as content.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Promotion group'),
);

/**
 *
 */
function stone_promotion_groups_promotion_group_content_type_content_type($subtype) {
  return _stone_promotion_groups_promotion_group_get_info($subtype);
}

/**
 * Return all promotion content types available.
 */
function stone_promotion_groups_promotion_group_content_type_content_types() {
  return _stone_promotion_groups_promotion_group_get_info();
}

/**
 *
 */
function _stone_promotion_groups_promotion_group_get_info($subtype = NULL) {
  $promotion_groups = array();

  $query = db_select('promotion_group', 'pg')
            ->fields('pg', array('pgid', 'type', 'title', 'status'))
            ->orderBy('pg.title');

  if (!empty($pgid)) {
    $query->condition('pgid', $subtype);
  }

  $result = $query->execute()->fetchAllAssoc('pgid');

  foreach ($result as $promotion_group) {
    $status = $promotion_group->status ? '' : ' (unpublished)';
    $promotion_groups[$promotion_group->pgid] = array(
      'title' => $promotion_group->title . $status,
      'icon' => 'icon_' . $promotion_group->type . '.png',
      'category' => t('Promotion groups'),
    );
  }

  if (array_key_exists($subtype, $promotion_groups)) {
    return $promotion_groups[$subtype];
  }

  return $promotion_groups;
}


/**
 * Output function for the 'promotion' content type. Outputs a promotion
 * based on the module and delta supplied in the configuration.
 */
function stone_promotion_groups_promotion_group_content_type_render($subtype, $conf) {
  if ($promotion_group = promotion_group_load($subtype)) {
    $block = new stdClass();
    $block->content = promotion_group_view($promotion_group);

    return $block;
  };

  return;
}

/**
 * Returns the administrative title for a type.
 */
function stone_promotion_groups_promotion_group_content_type_admin_title($subtype, $conf) {
  $promotion_group = _stone_promotion_groups_promotion_group_get_info($subtype);

  if (empty($promotion_group)) {
    return t('Deleted/missing promotion group @subtype_id', array('@subtype_id' => $subtype));
  }

  return t('Promotion group: @title', array('@title' => $promotion_group['title']));
}

/**
 *
 */
function stone_promotion_groups_promotion_group_content_type_admin_info($subtype, $conf) {
  $promotion_group = _stone_promotion_groups_promotion_group_get_info($subtype);

  if (!empty($promotion_group)) {
    $block = new stdClass;
    $block->title = $promotion_group['title'];
    $block->content = '';

    return $block;
  }
}
