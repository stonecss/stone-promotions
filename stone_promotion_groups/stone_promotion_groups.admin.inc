<?php

/**
 * Promotion Group Type form.
 */
function promotion_group_type_form($form, &$form_state, $promotion_group_type, $op = 'edit') {
  if ($op == 'clone') {
    $promotion_group_type->label .= ' (cloned)';
    $promotion_group_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $promotion_group_type->label,
    '#description' => t('The human-readable name of this promotion group type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($promotion_group_type->type) ? $promotion_group_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $promotion_group_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'stone_promotion_groups_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this promotion group type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($promotion_group_type->description) ? $promotion_group_type->description : '',
    '#description' => t('A description of this promotion group type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save promotion group type'),
    '#weight' => 40,
  );

  if (!$promotion_group_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete promotion group type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('promotion_group_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for the Promotion Group Type form.
 */
function promotion_group_type_form_submit(&$form, &$form_state) {
  $promotion_group_type = entity_ui_form_submit_build_entity($form, $form_state);

  // Save and go back.
  promotion_group_type_save($promotion_group_type);

  // Redirect user back to list of Promotion Group Types.
  $form_state['redirect'] = 'admin/structure/promotion-group-types';
}

/**
 * Submit handler for deleting a Promotion Group Type.
 */
function promotion_group_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/promotion-group-types/' . $form_state['promotion_group_type']->type . '/delete';
}

/**
 * Promotion Group Type delete confirm form.
 */
function promotion_group_type_delete_confirm_form($form, &$form_state, $promotion_group_type) {
  $form_state['promotion_group_type'] = $promotion_group_type;

  // Always provide entity id in the same form key as in the entity edit form.
  $form['promotion_group_type_id'] = array('#type' => 'value', '#value' => entity_id('promotion_group_type', $promotion_group_type));

  return confirm_form($form,
    t('Are you sure you want to delete promotion group type %title?', array('%title' => entity_label('promotion_group_type', $promotion_group_type))),
    'promotion/' . entity_id('promotion_group_type', $promotion_group_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for the Promotion Group Type delete form.
 */
function promotion_group_type_delete_confirm_form_submit($form, &$form_state) {
  $promotion_group_type = $form_state['promotion_group_type'];
  promotion_group_type_delete($promotion_group_type);

  watchdog('promotion_group_type', '@type: deleted %title.', array('@type' => $promotion_group_type->type, '%title' => $promotion_group_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $promotion_group_type->type, '%title' => $promotion_group_type->label)));

  $form_state['redirect'] = 'admin/structure/promotion-group-types';
}

/**
 * Page callback for /promotion-group/add.
 */
function stone_promotion_groups_admin_add_page() {
  $items = array();

  foreach (stone_promotion_groups_types() as $promotion_group_type_key => $promotion_group_type) {
    $items[] = l(entity_label('promotion_group_type', $promotion_group_type), 'promotion-group/add/' . $promotion_group_type_key);
  }

  return array('list' => array('#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of promotion group to create.')));
}

/**
 * Page callback for /promotion-group/add/%promotion_group_type.
 */
function stone_promotion_groups_add($type) {
  $promotion_group_type = stone_promotion_groups_types($type);

  $promotion_group = entity_create('promotion_group', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('promotion_group_type', $promotion_group_type))));

  return drupal_get_form('promotion_group_form', $promotion_group);
}

/**
 * Promotion Group form.
 */
function promotion_group_form($form, &$form_state, $promotion_group) {
  $form_state['promotion_group'] = $promotion_group;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $promotion_group->title,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $promotion_group->uid,
  );

  field_attach_form('promotion_group', $promotion_group, $form, $form_state);

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => $promotion_group->status,
    '#weight' => 100,
  );

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save promotion group'),
    '#submit' => $submit + array('promotion_group_form_submit'),
  );

  // Show the delete button if we're editing a promotion.
  $promotion_group_id = entity_id('promotion_group', $promotion_group);

  if (!empty($promotion_group_id) && promotion_group_access('edit', $promotion_group)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('promotion_group_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'promotion_group_form_validate';
  return $form;
}

/**
 * Validation handler for the promotion group form.
 */
function promotion_group_form_validate($form, &$form_state) {

}

/**
 * Submit handler for the promotion group form.
 */
function promotion_group_form_submit($form, &$form_state) {
  $promotion_group = $form_state['promotion_group'];

  entity_form_submit_build_entity('promotion_group', $promotion_group, $form, $form_state);
  promotion_group_save($promotion_group);

  $promotion_group_uri = entity_uri('promotion_group', $promotion_group);
  $form_state['redirect'] = $promotion_group_uri['path'];

  drupal_set_message(t('promotion group %title saved.', array('%title' => entity_label('promotion_group', $promotion_group))));
}

/**
 * Submit handler for deleting a promotion.
 */
function promotion_group_form_submit_delete($form, &$form_state) {
  $promotion_group = $form_state['promotion_group'];
  $promotion_group_uri = entity_uri('promotion_group', $promotion_group);
  $form_state['redirect'] = $promotion_group_uri['path'] . '/delete';
}

/**
 * Promotion Group delete confirm form.
 */
function promotion_group_delete_confirm_form($form, &$form_state, $promotion_group) {
  $form_state['promotion_group'] = $promotion_group;

  // Always provide entity id in the same form key as in the entity edit form.
  $form['promotion_group_type_id'] = array('#type' => 'value', '#value' => entity_id('promotion_group', $promotion_group));
  $promotion_group_uri = entity_uri('promotion_group', $promotion_group);

  return confirm_form($form,
    t('Are you sure you want to delete promotion group%title?', array('%title' => entity_label('promotion_group', $promotion_group))),
    $promotion_group_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for the promotion group delete confirm form.
 */
function promotion_group_delete_confirm_form_submit($form, &$form_state) {
  $promotion_group = $form_state['promotion_group'];
  promotion_group_delete($promotion_group);

  drupal_set_message(t('Promotion Group %title deleted.', array('%title' => entity_label('promotion_group', $promotion_group))));

  $form_state['redirect'] = '<front>';
}
