<div<?php print $attributes; ?>>
  <div class="PromotionGroup-progress"><div class="inner"></div></div>

  <?php if ($show_controls): ?>
  <div class="PromotionGroup-controls">
    <div class="PromotionGroup-prev icon--angle-left"></div>
    <div class="PromotionGroup-pager">
      <?php foreach ($promotions as $promotion): ?>
        <span class="icon--circle"></span>
      <?php endforeach; ?>
    </div>
    <div class="PromotionGroup-next icon--angle-right"></div>
  </div>
  <?php endif; ?>

  <?php foreach ($promotions as $promotion): ?>
    <?php print render($promotion); ?>
  <?php endforeach; ?>
</div>
