<?php
/**
 * @file
 * stone_promotions.features.inc
 */

/**
 * Implements hook_views_api().
 */
function stone_promotions_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_promotion_type().
 */
function stone_promotions_default_promotion_type() {
  $items = array();
  $items['image'] = entity_import('promotion_type', '{ "type" : "image", "label" : "Image", "weight" : 0, "description" : "" }');
  $items['text'] = entity_import('promotion_type', '{ "type" : "text", "label" : "Text", "weight" : 0, "description" : "" }');
  return $items;
}
