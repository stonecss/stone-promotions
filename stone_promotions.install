<?php

/**
 * Implements hook_schema().
 */
function stone_promotions_schema() {
  $schema['promotion'] = array(
    'description' => 'The base table for promotions.',
    'fields' => array(
      'pid' => array(
        'description' => 'The primary identifier for the promotion.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The type (bundle) of this promotion.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'title' => array(
        'description' => 'The title of the promotion.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'cta' => array(
        'description' => 'The call to action of the promotion.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'url' => array(
        'description' => 'The url of the promotion.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'days' => array(
        'type' => 'varchar',
        'description' => "Days of the week this promotion should be displayed on",
        'length' => 20,
        'not null' => TRUE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'The {users}.uid that owns this promotion; initially, this is the user that created it.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'Boolean indicating whether the promotion is published (visible to non-administrators).',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the promotion was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the promotion was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('pid'),
  );

  $schema['promotion_type'] = array(
    'description' => 'Stores information about all defined promotion types.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique promotion type ID.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );

  return $schema;
}

/**
 * Add day field to {promotion} table.
 */
function stone_promotions_update_7000() {
  $field = array(
    'type' => 'varchar',
    'description' => "Days of the week this promotion should be displayed on",
    'length' => 20,
    'not null' => TRUE,
    'default' => '',
  );
  db_add_field('promotion', 'days', $field);
}
