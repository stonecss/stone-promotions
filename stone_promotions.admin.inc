<?php

/**
 * Promotion Type form.
 */
function promotion_type_form($form, &$form_state, $promotion_type, $op = 'edit') {
  if ($op == 'clone') {
    $promotion_type->label .= ' (cloned)';
    $promotion_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $promotion_type->label,
    '#description' => t('The human-readable name of this promotion type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($promotion_type->type) ? $promotion_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $promotion_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'stone_promotions_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this promotion type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($promotion_type->description) ? $promotion_type->description : '',
    '#description' => t('A description of this promotion type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save promotion type'),
    '#weight' => 40,
  );

  if (!$promotion_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete promotion type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('promotion_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for the promotion Type form.
 */
function promotion_type_form_submit(&$form, &$form_state) {
  $promotion_type = entity_ui_form_submit_build_entity($form, $form_state);

  // Save and go back.
  promotion_type_save($promotion_type);

  // Redirect user back to list of promotion Types.
  $form_state['redirect'] = 'admin/structure/promotion-types';
}

/**
 * Submit handler for deleting a promotion Type.
 */
function promotion_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/promotion-types/' . $form_state['promotion_type']->type . '/delete';
}

/**
 * Promotion Type delete confirm form.
 */
function promotion_type_delete_confirm_form($form, &$form_state, $promotion_type) {
  $form_state['promotion_type'] = $promotion_type;

  // Always provide entity id in the same form key as in the entity edit form.
  $form['promotion_type_id'] = array('#type' => 'value', '#value' => entity_id('promotion_type', $promotion_type));

  return confirm_form($form,
    t('Are you sure you want to delete promotion type %title?', array('%title' => entity_label('promotion_type', $promotion_type))),
    'promotion/' . entity_id('promotion_type', $promotion_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for the promotion Type delete form.
 */
function promotion_type_delete_confirm_form_submit($form, &$form_state) {
  $promotion_type = $form_state['promotion_type'];
  promotion_type_delete($promotion_type);

  watchdog('promotion_type', '@type: deleted %title.', array('@type' => $promotion_type->type, '%title' => $promotion_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $promotion_type->type, '%title' => $promotion_type->label)));

  $form_state['redirect'] = 'admin/structure/promotion-types';
}

/**
 * Page callback for /promotion/add.
 */
function stone_promotions_admin_add_page() {
  $items = array();

  foreach (stone_promotions_types() as $promotion_type_key => $promotion_type) {
    $items[] = l(entity_label('promotion_type', $promotion_type), 'promotion/add/' . $promotion_type_key);
  }

  return array('list' => array('#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of promotion to create.')));
}

/**
 * Page callback for /promotion/add/%promotion_type.
 */
function stone_promotions_add($type) {
  $promotion_type = stone_promotions_types($type);

  $promotion = entity_create('promotion', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('promotion_type', $promotion_type))));

  return drupal_get_form('promotion_form', $promotion);
}

/**
 * Promotion form.
 */
function promotion_form($form, &$form_state, $promotion) {
  $form_state['promotion'] = $promotion;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Title'),
    '#default_value' => $promotion->title,
  );

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#default_value' => $promotion->url,
  );

  $form['cta'] = array(
    '#type' => 'textfield',
    '#title' => t('CTA'),
    '#attributes' => array(
      'placeholder' => t('e.g. Sign up'),
    ),
    '#default_value' => $promotion->cta,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $promotion->uid,
  );

  field_attach_form('promotion', $promotion, $form, $form_state);

  $form['days'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Days'),
    '#description' => t('Only show the promotion on the selected days'),
    '#options' => stone_promotions_days_of_the_week(),
    '#default_value' => !empty($promotion->days) ? array_keys($promotion->days) : array(),
    '#weight' => 100,
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => $promotion->status,
    '#weight' => 100,
  );

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save promotion'),
    '#submit' => $submit + array('promotion_form_submit'),
  );

  // Show the delete button if we're editing a promotion.
  $promotion_id = entity_id('promotion', $promotion);

  if (!empty($promotion_id) && promotion_access('edit', $promotion)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('promotion_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'promotion_form_validate';
  return $form;
}

/**
 * Validation handler for the promotion form.
 */
function promotion_form_validate($form, &$form_state) {
  // Validate url
  if (!empty($form_state['values']['url']) && link_validate_url(trim($form_state['values']['url'])) == FALSE) {
    form_set_error('url', t('The value provided is not a valid URL.'));
  }
}

/**
 * Submit handler for the promotion form.
 */
function promotion_form_submit($form, &$form_state) {
  $form_state['values']['days'] = join(',', array_filter($form_state['values']['days']));

  $form_state['values']['url'] = link_cleanup_url($form_state['values']['url']);
  if (link_validate_url($form_state['values']['url']) == LINK_INTERNAL && $path = drupal_lookup_path('source', $form_state['values']['url'])) {
    $form_state['values']['url'] = $path;
  }

  $promotion = $form_state['promotion'];
  entity_form_submit_build_entity('promotion', $promotion, $form, $form_state);
  promotion_save($promotion);

  $promotion_uri = entity_uri('promotion', $promotion);
  $form_state['redirect'] = $promotion_uri['path'];

  drupal_set_message(t('promotion %title saved.', array('%title' => entity_label('promotion', $promotion))));
}

/**
 * Submit handler for deleting a promotion.
 */
function promotion_form_submit_delete($form, &$form_state) {
  $promotion = $form_state['promotion'];
  $promotion_uri = entity_uri('promotion', $promotion);
  $form_state['redirect'] = $promotion_uri['path'] . '/delete';
}

/**
 * Promotion delete confirm form.
 */
function promotion_delete_confirm_form($form, &$form_state, $promotion) {
  $form_state['promotion'] = $promotion;

  // Always provide entity id in the same form key as in the entity edit form.
  $form['promotion_type_id'] = array('#type' => 'value', '#value' => entity_id('promotion', $promotion));
  $promotion_uri = entity_uri('promotion', $promotion);

  return confirm_form($form,
    t('Are you sure you want to delete promotion %title?', array('%title' => entity_label('promotion', $promotion))),
    $promotion_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for the promotion delete confirm form.
 */
function promotion_delete_confirm_form_submit($form, &$form_state) {
  $promotion = $form_state['promotion'];
  promotion_delete($promotion);

  drupal_set_message(t('Promotion %title deleted.', array('%title' => entity_label('promotion', $promotion))));

  $form_state['redirect'] = '<front>';
}
