<?php

/**
 * @file
 * Provide promotions as content.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Promotion'),
);

/**
 *
 */
function stone_promotions_promotion_content_type_content_type($subtype) {
  return _stone_promotions_promotion_get_info($subtype);
}

/**
 * Return all promotion content types available.
 */
function stone_promotions_promotion_content_type_content_types() {
  return _stone_promotions_promotion_get_info();
}

/**
 *
 */
function _stone_promotions_promotion_get_info($subtype = NULL) {
  $promotions = array();

  $query = db_select('promotion', 'p')
            ->fields('p', array('pid', 'type', 'title', 'status'))
            ->orderBy('p.title');

  if (!empty($pid)) {
    $query->condition('pid', $subtype);
  }

  $result = $query->execute()->fetchAllAssoc('pid');

  foreach ($result as $promotion) {
    $status = $promotion->status ? '' : ' (unpublished)';
    $promotions[$promotion->pid] = array(
      'title' => $promotion->title . $status,
      'icon' => 'icon_' . $promotion->type . '.png',
      'category' => t('Promotions'),
    );
  }

  if (array_key_exists($subtype, $promotions)) {
    return $promotions[$subtype];
  }

  return $promotions;
}


/**
 * Output function for the 'promotion' content type. Outputs a promotion
 * based on the module and delta supplied in the configuration.
 */
function stone_promotions_promotion_content_type_render($subtype, $conf) {
  if ($promotion = promotion_load($subtype)) {
    $block = new stdClass();
    $block->content = promotion_view($promotion);

    return $block;
  };

  return;
}

/**
 * Returns the administrative title for a type.
 */
function stone_promotions_promotion_content_type_admin_title($subtype, $conf) {
  $promotion = _stone_promotions_promotion_get_info($subtype);

  if (empty($promotion)) {
    return t('Deleted/missing promotion @subtype_id', array('@subtype_id' => $subtype));
  }

  return t('Promotion: @title', array('@title' => $promotion['title']));
}

/**
 *
 */
function stone_promotions_promotion_content_type_admin_info($subtype, $conf) {
  $promotion_info = _stone_promotions_promotion_get_info($subtype);

  if (!empty($promotion_info)) {
    $block = new stdClass;
    $block->title = $promotion_info['title'];
    $block->content = '';

    return $block;
  }
}
