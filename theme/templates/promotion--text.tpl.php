<div<?php print $attributes; ?>>
  <?php if ($link): ?><a<?php print $link_attributes; ?>><?php endif; ?>
    <h2 class="Promotion-title"><?php print $title; ?></h2>
    <div class="Promotion-body"><?php print $body; ?></div>
    <?php if ($cta): ?>
      <div class="Promotion-cta button"><?php print $cta; ?></div>
    <?php endif; ?>
  <?php if ($link): ?></a><?php endif; ?>
</div>
